import pytest


# Function to execute test cases by passing the input data as parameters to the fixture
@pytest.mark.parametrize("user,password", [("User1", "Pwd1"), ("User2", "Pwd2"), ("User3", "Pwd3"), ("User4", "Pwd4")])
def test_findresults(userandpwd):
    print(userandpwd)


# Command to execute
# pytest -vs

# Command to generate html report
# pytest --html=report.html
