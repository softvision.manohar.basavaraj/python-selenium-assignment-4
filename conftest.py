import pytest


# Fixture with params to accept the set of inputs from a pytest method
@pytest.fixture(scope="function", params=["user,password"])
def userandpwd(user, password):
    return user, password
